**jira-sql-data-access**
==============
Spike of access to data from the Jira SQL Server database captured via Custom Fields on a Jira Issue
Dependencies
--------------
- Microsoft JDBC Driver for SQL Server 4+ (sqljdbc4.jar)

Usage
--------------
    java -cp .:./sqljdbc4.jar JdbcSQLServerAccess <dbserver> <dbname> <dbuser> <dbpwd> <delimiter> <displayHeaders> <sqlfile|sqlstmt> 
Arguments
--------------
- **dbserver** : SQL Server Instance with Port *(eg "ANALYTIC_PRD_AG:50200")*
- **dbname** : Database Name *(eg "jira_prod")*
- **dbuser** : Database User Name *(eg "ana_dbo_jira")*
- **dbpwd** : Database User Password
- **delimiter** : Column Delimiter *(eg "|")*
- **displayHeaders** : Display Column Names in First Row? *(true|false)*
- **sqlfile or sqlstmt** : File with SQL Server Query to Execute *(eg "jira-issues.sql" or "SELECT * FROM jira.jiraissue")*
References
--------------
[Jira Database Schema](https://developer.atlassian.com/jiradev/files/4227160/JIRA61_db_schema.pdf)   
[Jira Example Queries](https://confluence.atlassian.com/display/JIRA041/Example+SQL+queries+for+JIRA#ExampleSQLqueriesforJIRA-ReturnAllProjectIssues)
