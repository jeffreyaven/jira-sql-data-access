--
--Jira Issues
--
SELECT 
ji.id AS issue_id 
,ji.description as issue_description 
,ji.summary AS issue_summary 
,ist.pname as issue_status 
,ji.CREATED as created 
,ji.UPDATED as updated 
FROM jira.project p 
LEFT OUTER JOIN jira.jiraissue ji ON ji.project = p.id 
LEFT OUTER JOIN jira.issuestatus ist ON ji.issuestatus = ist.id 
WHERE p.pname = 'Campaign Brief Entry' 
AND ist.pname = 'Deployment'
ORDER BY ji.pkey


