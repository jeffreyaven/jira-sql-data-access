--
--Custom Field Values
--
SELECT ji.id AS campaign_id 
, cf.cfname AS field_name 
, CASE REPLACE(cf.customfieldtypekey,'com.atlassian.jira.plugin.system.customfieldtypes:','')  
	WHEN 'datepicker' THEN CAST(cfv.datevalue AS nvarchar) 
	WHEN 'float' THEN CAST(cfv.numbervalue AS nvarchar) 
	WHEN 'labels' THEN cfv.STRINGVALUE 
	WHEN 'multicheckboxes' THEN cfo.customvalue 
	WHEN 'multiselect' THEN cfo.customvalue 
	WHEN 'radiobuttons' THEN cfo.customvalue 
	WHEN 'select' THEN cfo.customvalue  
	WHEN 'textarea' THEN cfv.textvalue 
	WHEN 'textfield' THEN cfv.STRINGVALUE  
	ELSE cfv.STRINGVALUE 
END AS field_value
, REPLACE(cf.customfieldtypekey,'com.atlassian.jira.plugin.system.customfieldtypes:','') AS field_type 
FROM jira.project p 
INNER JOIN jira.jiraissue ji ON ji.project = p.id 
LEFT OUTER JOIN jira.issuestatus ist ON ji.issuestatus = ist.id
INNER JOIN jira.customfieldvalue cfv ON cfv.issue = ji.id 
INNER JOIN jira.customfield cf ON cf.ID = cfv.customfield 
LEFT OUTER JOIN 
(SELECT CAST(id AS nvarchar) AS id_as_str, customvalue FROM jira.customfieldoption) cfo 
ON cfv.STRINGVALUE = cfo.id_as_str   
WHERE p.pname = 'Campaign Brief Entry' 
AND ist.pname = 'Deployment'
ORDER BY ji.pkey