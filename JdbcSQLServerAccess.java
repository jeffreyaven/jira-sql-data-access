import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.rowset.JdbcRowSet;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.io.IOException;

public class JdbcSQLServerAccess
{
public static void main(String[] args) throws IOException	
  //public static void main(String[] args)
  {
    // Usage
	// JdbcSQLServerAccess <dbserver> <dbname> <dbuser> <dbpwd> <delimiter> <displayHeaders> <sqlfile|sqlstatement> 
	Charset ENCODING = StandardCharsets.UTF_8;	
	
	Connection conn = null;
    try
    {
		String dbserver = args[0];
		String dbname = args[1];
		String dbuser = args[2];	
		String dbpwd = args[3];
		String delimiter = args[4];
		String displayHeaders = args[5];
		String sqlfile = args[6];
		String sql = "";		
		
		if (sqlfile.startsWith("SELECT ")){
			//input type is a sql statement
			sql = sqlfile;
		} else {	
			//input is a file	
			Path path = Paths.get(sqlfile);  
			List<String> lines = Files.readAllLines(path, ENCODING);

			for(String line : lines)  {
				if (!line.startsWith("--")) {
					sql = sql + " " + line;
						
				}
			}
			//System.out.println(sql);
		}
		
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

		String url = "jdbc:sqlserver://" + dbserver + ";DatabaseName=" + dbname;

		conn = DriverManager.getConnection(url,dbuser, dbpwd);
	   
		Statement state=conn.createStatement();
		ResultSet result = state.executeQuery(sql);
		
		ResultSetMetaData rsmd = result.getMetaData();
	   	int numberOfColumns = rsmd.getColumnCount();

		if (displayHeaders.equals("true")) {		
			for (int i = 1; i <= numberOfColumns; i++) {
				if (i > 1) System.out.print(delimiter);
					String columnName = rsmd.getColumnName(i);
					System.out.print(columnName);
				}
			System.out.println("");
			}
		
		while (result.next()) {
			for (int i = 1; i <= numberOfColumns; i++) {
				if (i > 1) System.out.print(delimiter);
					String columnValue = result.getString(i);
					System.out.print(columnValue);
				}
			System.out.println("");  
		}
		state.close();
		conn.close();   
    }
    catch (ClassNotFoundException e)
    {
      e.printStackTrace();
      System.exit(1);
    }
    catch (SQLException e)
    {
      e.printStackTrace();
      System.exit(2);
    }
  }
}


//javac JdbcSQLServerAccess.java 
//java -cp .:./sqljdbc4.jar JdbcSQLServerAccess "ANALYTIC_PRD_AG:50200" "jira_prod" "ana_dbo_jira" "*@na_Db0_j1R@" "|" "true" "jira-issues.sql"
